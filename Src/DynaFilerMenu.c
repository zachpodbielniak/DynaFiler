#include <string.h>
#include <nautilus-extension.h>
#include "DynaFilerMenu.h"
#include "DynaFilerEntityInfo.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>




struct _DynaFiler
{
	GObject 		parent_instance;
};




static 
void 
menu_provider_iface_init(
	NautilusMenuProviderInterface 		*iface
);




G_DEFINE_DYNAMIC_TYPE_EXTENDED(
	DynaFiler, 
	dynafiler, 
	G_TYPE_OBJECT, 
	0,
	G_IMPLEMENT_INTERFACE_DYNAMIC(
		NAUTILUS_TYPE_MENU_PROVIDER, 
		menu_provider_iface_init
	)
);




static 
NautilusMenuItem *
generate_dynatrace_agent_details_menu(
	NautilusMenuProvider 			*provider,
	GtkWidget            			*window,
	GList                			*files
){
	NautilusMenuItem *item;
	gboolean one_item;

	if (files == NULL)
	{ return NULL; }

	one_item = (files != NULL) && (files->next == NULL);
	if (one_item && !nautilus_file_info_is_directory((NautilusFileInfo *)files->data))
	{
		item = nautilus_menu_item_new(
			"DynaFiler::EntityInformation",
			"Dynatrace Entity Info",
			"Show Dynatrace Entity Info For This File",
			NULL
		);
	}
	else
	{ return NULL; }
	
	g_signal_connect(
		item,
		"activate",
		G_CALLBACK(dynafiler_get_agent_details_callback),
		provider
	);

	g_object_set_data_full(
		G_OBJECT(item),
		"files",
		nautilus_file_info_list_copy(files),
		(GDestroyNotify)nautilus_file_info_list_free
	);
 
	return item;
}



static 
GList *
get_file_items(
	NautilusMenuProvider 			*provider,
	GtkWidget            			*window,
	GList                			*files
){
	DynaFiler *df;
	GList *items;
	NautilusMenuItem *get_agent_details;


	df = NULL;
	items = NULL;
	get_agent_details = NULL;

	df = NAUTILUS_DYNAFILER(provider);
	
	/* Agent Entity Info */
	get_agent_details = generate_dynatrace_agent_details_menu(provider, window, files);
	items = (NULL != get_agent_details) ? g_list_append(items, get_agent_details) : items;
	

	return items;
}




static 
void
menu_provider_iface_init(
	NautilusMenuProviderInterface 		*iface
){ 
	iface->get_file_items = get_file_items; 
	/* iface->get_background_items = get_background_items; */
}




static 
void
dynafiler_init(
	DynaFiler 				*df
){ (void)0; }




static 
void
dynafiler_class_init(
	DynaFilerClass 				*klass
){ (void)0; }




static 
void
dynafiler_class_finalize(
	DynaFilerClass 				*klass
){ (void)0; }




void
dynafiler_load(
	GTypeModule 				*module
){ dynafiler_register_type(module); }
