#include "DynaFilerEntityInfo.h"
#include "RegexConstants.h"


#define SAAS_DOMAIN		".dev.dynatracelabs.com"



typedef struct _DynaFilerEntitiesInfo
{
	GFile 			*file;
	GString			*file_data;

	gchar 			*tenant_id;
	gchar 			*env_id;
	gchar			*pgi_id;
	gchar			*pg_id;
	gchar 			*osi_id;
	gchar			*agent_id;
	gchar			*agent_version;
	gchar 			*host_name;
	gchar 			*injection_mode;
	gchar 			*standalone;

	gboolean		is_saas;
} DynaFilerEntitiesInfo;




void
dynafiler_show_message_box_error(
	const gchar			*title,
	const gchar			*message
){
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new(
		NULL,
		GTK_DIALOG_MODAL,
		GTK_MESSAGE_ERROR,
		GTK_BUTTONS_OK,
		title,
		NULL
	);

	gtk_message_dialog_format_secondary_text(
		GTK_MESSAGE_DIALOG(dialog),
		message,
		NULL
	);

	g_signal_connect(dialog, "response", G_CALLBACK(gtk_window_close), NULL);
	gtk_widget_show(GTK_WIDGET(dialog));

}



static
GString *
dynafiler_generate_environment_link_for_label(
	const gchar				*env_link
){
	GString *env_hyperlink;

	env_hyperlink = g_string_new("<span><a href=\"");

	g_string_append(env_hyperlink, env_link);
	g_string_append(env_hyperlink, "\" title=\"Environment Link\">Environment Link</a> - ");
	g_string_append(env_hyperlink, env_link);
	g_string_append(env_hyperlink, "</span>");

	return env_hyperlink;
}



static
GString *
dynafiler_generate_environment_agent_text_for_label(
	const gchar				*env_link,
	const gchar				*agent_id
){
	GString *ret;

	ret = g_string_new("Agent ID - ");
	g_string_append(ret, agent_id);

	return ret;
}




static
GString *
dynafiler_generate_environment_agent_version_text_for_label(
	const gchar				*env_link,
	const gchar				*agent_version
){
	GString *ret;

	ret = g_string_new("Agent Version - ");
	g_string_append(ret, agent_version);

	return ret;
}




static
GString *
dynafiler_generate_osi_link_for_label(
	const gchar				*env_link,
	const gchar 				*osi_id
){
	GString *ret;

	ret = g_string_new("<span><a href=\"");
	g_string_append(ret, env_link);
	g_string_append(ret, "#newhosts/hostdetails;id=HOST-");
	g_string_append(ret, (const gchar *)((gsize)osi_id + 2));
	g_string_append(ret, "\" title=\"Link To Host\">Host Link</a> - ");
	g_string_append(ret, osi_id);
	g_string_append(ret, "</span>");

	return ret;
}




static
GString *
dynafiler_generate_pg_link_for_label(
	const gchar				*env_link,
	const gchar 				*pg_id
){
	GString *ret;

	ret = g_string_new("<span><a href=\"");
	g_string_append(ret, env_link);
	g_string_append(ret, "#processgroupdetails;id=PROCESS_GROUP-");
	g_string_append(ret, (const gchar *)((gsize)pg_id + 2));
	g_string_append(ret, "\" title=\"Link To PG\">PG Link</a> - ");
	g_string_append(ret, pg_id);
	g_string_append(ret, "</span>");

	return ret;
}




static
GString *
dynafiler_generate_pgi_link_for_label(
	const gchar				*env_link,
	const gchar 				*pgi_id
){
	GString *ret;

	ret = g_string_new("<span><a href=\"");
	g_string_append(ret, env_link);
	g_string_append(ret, "#processdetails;id=PROCESS_GROUP_INSTANCE-");
	g_string_append(ret, (const gchar *)((gsize)pgi_id + 2));
	g_string_append(ret, "\" title=\"Link To PGI\">PGI Link</a> - ");
	g_string_append(ret, pgi_id);
	g_string_append(ret, "</span>");

	return ret;
}




static 
GtkWidget *
dynafiler_create_label_with_link(
	const gchar				*text,
	gint					margin
){
	GtkWidget *ret;

	ret = gtk_label_new(text);
	gtk_label_set_use_markup(GTK_LABEL(ret), TRUE);
	gtk_label_set_max_width_chars(GTK_LABEL(ret), 40);
	gtk_label_set_xalign(GTK_LABEL(ret), 0.0);
	gtk_widget_set_margin_start(ret, margin);
	gtk_widget_set_margin_end(ret, margin);
	gtk_widget_set_margin_top(ret, margin);
	gtk_widget_set_margin_bottom(ret, margin);

	return ret;
}




static
void
dynafiler_get_agent_details_window(
	const gchar				*file_name,
	const gchar 				*env_link,
	const gchar 				*agent_id,
	const gchar				*agent_version,
	const gchar 				*osi_id,
	const gchar				*pg_id,
	const gchar				*pgi_id
){
	g_return_if_fail(env_link);

	GtkWidget *window;
	GtkWidget *vbox;
	GtkWidget *label_env;
	GtkWidget *label_agent_id;
	GtkWidget *label_agent_version;
	GtkWidget *label_osi;
	GtkWidget *label_pg;
	GtkWidget *label_pgi;
	gint margin;

	margin = 5;

	/* Window Creation */
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	/* Window Customization */
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_title(GTK_WINDOW(window), file_name);
	gtk_window_set_resizable(GTK_WINDOW(window), TRUE);
	g_object_add_weak_pointer(G_OBJECT(window), (gpointer *)&window);

	/* Create vbox */
	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);

	/* Environment Link Label */
	label_env = dynafiler_create_label_with_link(env_link, margin);

	/* Agent ID */
	label_agent_id = dynafiler_create_label_with_link(agent_id, margin);
	
	/* Agent Version */
	label_agent_version = dynafiler_create_label_with_link(agent_version, margin);

	/* OSI ID */
	label_osi = dynafiler_create_label_with_link(osi_id, margin);
	
	/* PG ID */
	label_pg = dynafiler_create_label_with_link(pg_id, margin);

	/* PGI Info Label */
	label_pgi = dynafiler_create_label_with_link(pgi_id, margin);

	/* Box Packing */
	gtk_box_pack_start(GTK_BOX(vbox), label_env, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), label_osi, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), label_pg, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), label_pgi, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), label_agent_id, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), label_agent_version, FALSE, FALSE, 0);

	/* Present the full window */
	gtk_widget_show_all(window);
}



static 
gchar *
dynafiler_extract_value_from_regex(
	DynaFilerEntitiesInfo			*info,
	const gchar				*regex_option,
	const gchar 				*shim
){
	GRegex *regex;
	GMatchInfo *match;
	gchar *line;
	gchar *ret;

	/* Main regex */
	regex = g_regex_new(regex_option, 0, 0, NULL);
	g_regex_match(regex, info->file_data->str, G_REGEX_MATCH_NEWLINE_ANY, &match);
	while (g_match_info_matches(match))
	{
		line = g_match_info_fetch(match, 0);
		g_print("%s\n", line);
		g_match_info_next(match, NULL);
	}

	g_match_info_free(match);
	g_regex_unref(regex);

	if (NULL == line)
	{ return NULL; }

	
	/* Regex shimming */
	regex = g_regex_new(shim, 0, 0, NULL);
	g_regex_match(regex, line, G_REGEX_MATCH_NEWLINE_ANY, &match);
	while (g_match_info_matches(match))
	{
		ret = g_match_info_fetch(match, 0);
		g_print("%s\n", ret);
		g_match_info_next(match, NULL);
	}

	g_match_info_free(match);
	g_regex_unref(regex);


	g_free(line);
	return ret;
}



static
void 
dynafiler_entity_info_free(
	DynaFilerEntitiesInfo			*info
){
	g_return_if_fail(info);

	g_free(info->agent_id);
	g_free(info->agent_version);
	g_free(info->env_id);
	g_free(info->host_name);
	g_free(info->injection_mode);
	g_free(info->osi_id);
	g_free(info->pg_id);
	g_free(info->pgi_id);
	g_free(info->standalone);
	g_free(info->tenant_id);

	g_object_unref(info->file);
	g_string_free(info->file_data, FALSE);
	g_free(info);
}




static
DynaFilerEntitiesInfo *
dynafiler_build_entity_info(
	const gchar				*file_path
){
	g_return_val_if_fail(file_path, NULL);

	DynaFilerEntitiesInfo *info;
	g_autoptr(GFileInputStream) stream;
	gchar buffer[0x400];
	gssize read;

	info = g_malloc(sizeof(DynaFilerEntitiesInfo));
	memset(info, 0x00, sizeof(DynaFilerEntitiesInfo));
	memset(buffer, 0x00, sizeof(buffer));

	info->file_data = g_string_new(NULL);

	/* Example from https://stackoverflow.com/questions/40884738/file-handling-using-gio */
	info->file = g_file_new_for_path(file_path);
	stream = g_file_read(info->file, NULL, NULL);

	for (;;)
	{
		read = g_input_stream_read(
			G_INPUT_STREAM(stream),
			buffer,
			G_N_ELEMENTS(buffer) - 1,
			NULL,
			NULL
		);

		if (read > 0)
		{
			buffer[read] = '\0';
			g_string_append(info->file_data, buffer);

		}
		else break;
	}

	/* Now we have file data in info->file_data->str */

	/* Tenant ID */
	info->tenant_id = dynafiler_extract_value_from_regex(
		info,
		REGEX_AGENT_LOG_TENANT_UUID,
		REGEX_AGENT_LOG_TENANT_UUID_SHIM
	);

	/* Get the Env URL */
	memset(buffer, 0x00, sizeof(buffer));
	g_snprintf(
		buffer,
		sizeof(buffer) - 1,
		"https://%s%s/",
		info->tenant_id,
		SAAS_DOMAIN
	);

	info->env_id = g_strdup((const gchar *)buffer);

	/* Agent ID */
	info->agent_id = dynafiler_extract_value_from_regex(
		info,
		REGEX_AGENT_LOG_AGENT_ID,
		REGEX_AGENT_LOG_AGENT_ID_SHIM
	);

	/* Agent Version */
	info->agent_version = dynafiler_extract_value_from_regex(
		info,
		REGEX_AGENT_LOG_AGENT_VERSION,
		REGEX_AGENT_LOG_AGENT_VERSION_SHIM
	);
	
	/* OSI ID */
	info->osi_id = dynafiler_extract_value_from_regex(
		info,
		REGEX_AGENT_LOG_OSI_ID,
		REGEX_AGENT_LOG_OSI_ID_SHIM
	);
	
	/* PG ID */
	info->pg_id = dynafiler_extract_value_from_regex(
		info,
		REGEX_AGENT_LOG_PG_ID,
		REGEX_AGENT_LOG_PG_ID_SHIM
	);

	/* PGI ID */
	info->pgi_id = dynafiler_extract_value_from_regex(
		info,
		REGEX_AGENT_LOG_PGI_ID,
		REGEX_AGENT_LOG_PGI_ID_SHIM
	);
	
	



	return info;
}




void
dynafiler_get_agent_details_callback(
	NautilusMenuItem 			*item,
	gpointer				user_data
){
	GList *files;
	gchar *fileuri;
	gchar *filepath;
	gchar *filename;
	g_autoptr(GString) env_link;
	g_autoptr(GString) agent_id;
	g_autoptr(GString) agent_version;
	g_autoptr(GString) osi_link;
	g_autoptr(GString) pg_link;
	g_autoptr(GString) pgi_link;
	DynaFilerEntitiesInfo *info;

	files = g_object_get_data(G_OBJECT(item), "files");
	g_return_if_fail(item);

	fileuri = nautilus_file_info_get_uri(files->data);
	g_return_if_fail(fileuri);

	filepath = g_filename_from_uri(fileuri, NULL, NULL);
	g_return_if_fail(filepath);

	filename = g_path_get_basename(filepath);

	g_print(
		"Agent Details: \n"
		"File URI: %s\n"
		"File Path: %s\n",
		fileuri,
		filepath
	);

	info = dynafiler_build_entity_info(filepath);

	env_link = dynafiler_generate_environment_link_for_label(info->env_id);
	agent_id = dynafiler_generate_environment_agent_text_for_label(info->env_id, info->agent_id);
	agent_version = dynafiler_generate_environment_agent_version_text_for_label(info->env_id, info->agent_version);
	osi_link = dynafiler_generate_osi_link_for_label(info->env_id, info->osi_id);
	pg_link = dynafiler_generate_pg_link_for_label(info->env_id, info->pg_id);
	pgi_link = dynafiler_generate_pgi_link_for_label(info->env_id, info->pgi_id);

	dynafiler_get_agent_details_window(
		filename,
		env_link->str,
		agent_id->str,
		agent_version->str,
		osi_link->str,
		pg_link->str,
		pgi_link->str
	);

	g_free(filepath);
	dynafiler_entity_info_free(info);
}