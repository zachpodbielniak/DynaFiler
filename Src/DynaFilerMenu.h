#ifndef DYNAFILER_DYNAFILERMENU_H
#define DYNAFILER_DYNAFILERMENU_H


#include <glib-object.h>
G_BEGIN_DECLS




#define NAUTILUS_TYPE_DYNAFILER 	(dynafiler_get_type())




G_DECLARE_FINAL_TYPE(DynaFiler, dynafiler, NAUTILUS, DYNAFILER, GObject)




void
dynafiler_load(
	GTypeModule			*module
);




G_END_DECLS
#endif