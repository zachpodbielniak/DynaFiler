#ifndef DYNAFILER_REGEXCONSTANTS_H
#define DYNAFILER_REGEXCONSTANTS_H




#define REGEX_AGENT_LOG_TENANT_UUID		"(\\[native\\] Tenant UUID \\.{17} [a-z]{3}[0-9]{5})"
#define REGEX_AGENT_LOG_TENANT_UUID_SHIM	"([a-z]{3}[0-9]{5})"

#define REGEX_AGENT_LOG_PGI_ID			"(\\[native\\] Process group instance ID \\.{3} 0x[a-fA-F0-9]{1,16})"
#define REGEX_AGENT_LOG_PGI_ID_SHIM		"(0x[a-fA-F0-9]{1,16})"

#define REGEX_AGENT_LOG_AGENT_VERSION		"(\\[native\\] Version \\.{21} 1\\.\\d{2,}\\.\\d{1,}\\.\\d{8}-\\d{6}, build date)"
#define REGEX_AGENT_LOG_AGENT_VERSION_SHIM	"(1\\.\\d{3}\\.\\d{1,5}\\.\\d{8}-\\d{6})"

#define REGEX_AGENT_LOG_AGENT_ID		"(\\[native\\] Agent ID \\.{20} 0x[a-fA-F0-9]{1,16})"
#define REGEX_AGENT_LOG_AGENT_ID_SHIM		"(0x[a-fA-F0-9]{1,16})"

#define REGEX_AGENT_LOG_AGENT_HOST		"(\\[native\\] Agent host \\.{1,} (\\w|-){1,})"
#define REGEX_AGENT_LOG_AGENT_HOST_SHIM		"((\\w|-){1,})"

#define REGEX_AGENT_LOG_OSI_ID			"(\\[native\\] OSI ID \\.{22} 0x[a-fA-F0-9]{1,16})"
#define REGEX_AGENT_LOG_OSI_ID_SHIM		"(0x[a-fA-F0-9]{1,16})"

#define REGEX_AGENT_LOG_PG_ID			"(\\[native\\] Process group ID \\.{12} 0x[a-fA-F0-9]{1,16})"
#define REGEX_AGENT_LOG_PG_ID_SHIM		"(0x[a-fA-F0-9]{1,16})"

#define REGEX_AGENT_LOG_INJECTION_MODE		"(\\[native\\] Injection mode \\.{14})"
#define REGEX_AGENT_LOG_INJECTION_MODE_SHIM	"(\\[native\\] Injection mode \\.{14})"

#define REGEX_AGENT_LOG_STANDALONE		"(\\[native\\] Standalone \\.{18}) (yes|no))"
#define REGEX_AGENT_LOG_STANDALONE_SHIM		"((yes|no))"




#endif