#include <nautilus-extension.h>
#include "DynaFilerMenu.h"




void
nautilus_module_initialize(
	GTypeModule 				*module
){ dynafiler_load(module); }




void
nautilus_module_shutdown(
	void
){ (void)0; }




void
nautilus_module_list_types(
	const GType 				**types,
	int					*num_types
){
	static GType type_list[1];

	type_list[0] = NAUTILUS_TYPE_DYNAFILER;
	*types = type_list;

	*num_types = 1;
}
