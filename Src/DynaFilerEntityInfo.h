#ifndef DYNAFILER_HELPERFUNCS_H
#define DYNAFILER_HELPERFUNCS_H




#include <nautilus-extension.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>




const
char *
dynafiler_strip_protocol_from_full_uri(
	const char			*uri
);




void
dynafiler_show_message_box_error(
	const char			*title,
	const char			*message
);



void
dynafiler_open_with_code_callback(
	NautilusMenuItem 			*item,
	gpointer				user_data
);



void
dynafiler_get_agent_details_callback(
	NautilusMenuItem 			*item,
	gpointer				user_data
);



#endif