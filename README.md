# DynaFiler

A nautilus file manager extension that does magic with Dynatrace Support Archives. 


## How To Build
If on an ubuntu based machine (ubuntu, pop!_os, etc) you need to install the following package:
```
libnautilus-extension-dev
```

After installation validate that both *pkg-config* commands below provide valid output:
```
pkg-config libnautilus-extension --cflags
pkg-config libnautilus-extension --libs
```

Then you can run the following *make* commands:
```
make all
sudo make install
```
This is now installed in the nautilus extension folder. Nautilus will auto-load and initialize the shared-object at startup of nautilus.


### Uninstall
Run the following make command:
```
sudo make rm
```
This removes the extension from nautilus, so it will no longer be loaded.


### Debugging
Build the debug version first and install that (overwrites the "release" version)
```
make clean
make debug
sudo make install
```

Then launch *GDB* (or *CGDB* if you prefer) with nautilus:
```
nautilus -q
gdb $(which nautilus)
```
and then use *GDB* how you normally would. 

