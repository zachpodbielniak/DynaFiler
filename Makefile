
CFLAGS = `pkg-config libnautilus-extension --cflags`
LIBS = `pkg-config libnautilus-extension --libs`


all:
	mkdir -p bin/
	gcc -c Src/DynaFilerEntryPoint.c -o bin/DynaFilerEntryPoint.o $(CFLAGS) -O3
	gcc -c Src/DynaFilerMenu.c -o bin/DynaFilerMenu.o $(CFLAGS) -O3
	gcc -c Src/DynaFilerEntityInfo.c -o bin/DynaFilerEntityInfo.o $(CFLAGS) -O3
	gcc -shared bin/*.o -o bin/libnautilus-dynafiler.so $(LIBS) -O3
	rm bin/*.o

debug:
	mkdir -p bin/
	gcc -g -c Src/DynaFilerEntryPoint.c -o bin/DynaFilerEntryPoint.o $(CFLAGS)
	gcc -g -c Src/DynaFilerMenu.c -o bin/DynaFilerMenu.o $(CFLAGS)
	gcc -g -c Src/DynaFilerEntityInfo.c -o bin/DynaFilerEntityInfo.o $(CFLAGS)
	gcc -g -shared bin/*.o -o bin/libnautilus-dynafiler.so $(LIBS)
	rm bin/*.o

clean: 
	rm -rf bin/

rm:
	rm /usr/lib/x86_64-linux-gnu/nautilus/extensions-3.0/libnautilus-dynafiler.so

install:
	cp bin/libnautilus-dynafiler.so /usr/lib/x86_64-linux-gnu/nautilus/extensions-3.0
